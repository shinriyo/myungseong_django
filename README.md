明星テコンドーのDjango
====

Overview

## Description

明星テコンドーのDjango。

## Demo

N/A

## VS. 

N/A

## Requirement

Django 1.6.4

## Usage

### chaeck your Django varsion

```
python -c "import django; print(django.get_version())"
```

### start application

```
python manage.py runserver 0.0.0.0:8000
```

gunicorn
```
gunicorn --env DJANGO_SETTINGS_MODULE=jam.settings jam.wsgi --log-file -
```

translate
```
python manage.py compilemessages
```

update
```
cd jam
django-admin.py makemessages --all
```

add translate ex) english
```
cd jam
django-admin.py makemessages -l en
```

### Access URL

http://127.0.0.1:8000/entries/

## Install

```
pip install Django==1.6.4
pip install django-pagination==1.0.7
```

## Contribution

shinriyo

## Licence

N/A

## Author

[shinriyo](https://github.com/shinriyo/)

