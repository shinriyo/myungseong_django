#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.generic import ListView, DetailView, TemplateView
from django.shortcuts import render_to_response, get_object_or_404
from entries.models import Entry, Comment
from entries.forms import CommentForm
# TODO: 今後使う？
from django.http import HttpResponse, Http404
from django.utils.translation import get_language, activate, ugettext_lazy as _


class EntryView(ListView):
    context_object_name = "object_list"
    queryset = Entry.objects.all()


class EntryDetailView(DetailView):
    queryset = Entry.objects.all()
    model = Entry

    def get_object(self):
        """
        :return:
        """
        # Call the superclass
        object = super(EntryDetailView, self).get_object
        # Record the last accessed date
        # object.last_accessed = timezone.now()
        # object.save()
        # Return the object
        return object

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(EntryDetailView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['comment_list'] = Comment.objects.all()
        return context


def entry_detail(request, entry_id):
    '''
    指定したスレッドを表示する。
    参考
    http://www.gesource.jp/programming/python/django/t002.html
    :param request: リクエスト
    :param entry_id: エントリID
    '''
    # 表示するエントリ
    entry = get_object_or_404(Entry, pk=entry_id)

    # コメントの登録処理
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            # コメントを登録
            comment = form.save(cosmmit=False)
            comment.thread = entry
            comment.save()
            form = CommentForm()  # フォームの初期化
    else:
        form = CommentForm()
    # スレッドのコメント
    comment_list = entry.comment_set.all().order_by('id')
    return TemplateView.as_view(template_name='entries/entry_detail.html',
                                extra_context=
                                {'object': entry,
                                 'comment_list': comment_list,
                                 'form': form})
