#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from django.forms import ModelForm
from entries.models import Comment

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = ('entry', )