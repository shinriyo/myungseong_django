#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template
from django.utils.translation import ugettext

register = template.Library()


@register.simple_tag
def my_const(attr):
    """
    定数を使う。
    テンプレートで以下を記載して使う。
    {% load my_tags %}
    {% my_const 'CONST_NAME' %}
    :param attr:
    :return:
    """
    return eval("const." + attr)


@register.simple_tag
def get_yen(price):
    """
    円をつけて返す。
    テンプレートで以下を記載して使う。
    {% load my_tags %}
    {% get_yen 100 %}
    :param price:
    :return:
    """
    decimal_points = 3
    seperator = u','
    # ローカライズからyenを変換
    unit = ugettext("yen")
    str_price = str(price)
    parts = []

    if len(str_price) > decimal_points:
        # 桁区切りする
        while str_price:
            parts.append(str_price[-decimal_points:])
            str_price = str_price[:-decimal_points]
        # now we should have parts = ['345', '12']
        parts.reverse()
        # str_price should be u'12.345'
        str_price = seperator.join(parts)

    return str_price + unit


@register.simple_tag
def get_walk_time(time, time_type='m'):
    """
    徒歩◯分を表示させる
    :param time: 時間の数字
    :param time_type: 時間単位の種類
    """
    str_time = str(time)
    dic = {'s': 'seconds', 'm': 'minutes', 'h': 'hours'}
    unit = ugettext(dic[time_type])
    on_foot = ugettext('on foot')
    res = on_foot + str_time + unit
    return res

