#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class Entry(models.Model):
    """
    エントリのモデル
    """
    title = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    email = models.EmailField(help_text='A valid e-mail address, please.')
    content = models.TextField(max_length=1000)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/entries/detail/%i" % self.id


class News(models.Model):
    """
    ニュース
    """
    title = models.CharField(max_length=100)
    content = models.TextField(max_length=1000)
    date = models.DateField(max_length=None)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/entries/detail_news/%i" % self.id


class Comment(models.Model):
    """
    エントリに対するコメントのモデル
    """
    # スレッド
    entry = models.ForeignKey(Entry, verbose_name=u'スレッド')
    name = models.CharField(max_length=100, verbose_name=u'名前')
    email = models.EmailField(help_text='A valid e-mail address, please.')
    # メッセージ
    message = models.CharField(u'メッセージ', max_length=200, blank=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'コメント'

    def __unicode__(self):
        return self.message

    def get_absolute_url(self):
        return "/entries/detail/%i" % self.id