#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    ('sugita', 'shinriyo@gmail.com'),
)

MANAGERS = ADMINS

# for pagination
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
)
# for pagination (1ページの表示数)
PAGINATION_DEFAULT_PAGINATION = 2

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Tokyo'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ja'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
ROOT_PATH = os.path.dirname(__file__)
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    [os.path.join(ROOT_PATH, 'static')]
)

# List of finder classes that know how to find static files in
# various locations.
# STATICFILES_FINDERS = (
# 'django.contrib.staticfiles.finders.FileSystemFinder',
# 'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
#)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '280iz3!009aa%pqc5a=yc2)xf)-_15b*3pr)%whw2lwbyn3!#9'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    # ドキュメントにも書かれているが、ミドルウェアの位置には注意する(SessionMiddlwwareの後ろ、
    # CommonMiddlewareの前)。
    'django.middleware.locale.LocaleMiddleware',  # localize
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # for pagination
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.contrib.auth.middleware.AuthenticationMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

ROOT_URLCONF = 'apps.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'apps.wsgi.application'

# TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'))
from os.path import join

TEMPLATE_DIRS = (
    join(BASE_DIR, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'entries',
    'pagination',
    'django.contrib.comments',
    # 'apps.entries',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    # 'django.contrib.markup',
    'gunicorn',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

LOGIN_REDIRECT_URL = "/entries/"
LOGIN_URL = "/entries/login/"

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

USE_I18N = True
USE_L10N = True

USE_TZ = True

from django.utils.translation import ugettext_lazy as _

LANGUAGES = (
    ('en', _('English')),
    ('ja', _('Japanese')),
    ('ko', _('Korean')),
    ('fr', _('Franch')),
    ('vi', _('Vietnamese')),
    ('zh-cn', _('Simplified Chinese')),
)

# TODO:
# LANGUAGES = [('ar', u'Arabic'), ('az', u'Azerbaijani'), ('bg', u'Bulgarian'), ('bn', u'Bengali'), ('bs', u'Bosnian'), ('ca', u'Catalan'), ('cs', u'Czech'), ('cy', u'Welsh'), ('da', u'Danish'), ('de', u'German'), ('el', u'Greek'), ('en', u'English'), ('en-gb', u'British English'), ('eo', u'Esperanto'), ('es', u'Spanish'), ('es-ar', u'Argentinian Spanish'), ('es-mx', u'Mexican Spanish'), ('es-ni', u'Nicaraguan Spanish'), ('et', u'Estonian'), ('eu', u'Basque'), ('fa', u'Persian'), ('fi', u'Finnish'), ('fr', u'French'), ('fy-nl', u'Frisian'), ('ga', u'Irish'), ('gl', u'Galician'), ('he', u'Hebrew'), ('hi', u'Hindi'), ('hr', u'Croatian'), ('hu', u'Hungarian'), ('id', u'Indonesian'), ('is', u'Icelandic'), ('it', u'Italian'), ('ja', u'Japanese'), ('ka', u'Georgian'), ('kk', u'Kazakh'), ('km', u'Khmer'), ('kn', u'Kannada'), ('ko', u'Korean'), ('lt', u'Lithuanian'), ('lv', u'Latvian'), ('mk', u'Macedonian'), ('ml', u'Malayalam'), ('mn', u'Mongolian'), ('nb', u'Norwegian Bokmal'), ('ne', u'Nepali'), ('nl', u'Dutch'), ('nn', u'Norwegian Nynorsk'), ('pa', u'Punjabi'), ('pl', u'Polish'), ('pt', u'Portuguese'), ('pt-br', u'Brazilian Portuguese'), ('ro', u'Romanian'), ('ru', u'Russian'), ('sk', u'Slovak'), ('sl', u'Slovenian'), ('sq', u'Albanian'), ('sr', u'Serbian'), ('sr-latn', u'Serbian Latin'), ('sv', u'Swedish'), ('sw', u'Swahili'), ('ta', u'Tamil'), ('te', u'Telugu'), ('th', u'Thai'), ('tr', u'Turkish'), ('tt', u'Tatar'), ('uk', u'Ukrainian'), ('ur', u'Urdu'), ('vi', u'Vietnamese'), ('zh-cn', u'Simplified Chinese'), ('zh-tw', u'Traditional Chinese')]

"""
For Heroku
"""
# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration
STATIC_ROOT = 'staticfiles'

import socket

HOSTNAME = socket.gethostname()
URL_ROOT = HOSTNAME

# toshima.ne.jpは仕方ない
if 'local' in HOSTNAME or 'toshima.ne.jp' in HOSTNAME:
    # local dev
    DEBUG = True
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
else:
    # Heroku
    #DEBUG = False
    DEBUG = True
    import dj_database_url

    DATABASES = {
        'default': dj_database_url.config()
    }

TEMPLATE_DEBUG = DEBUG
