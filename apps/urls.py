#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # スラッシュだけで行けるよう
                       url(r'^$', include('entries.urls')),
                       url(r'^entries/', include('entries.urls')),
                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
                            (r'^static/(?P<path>.*)',
                             'django.views.static.serve',
                             {'document_root': settings.STATIC_ROOT,
                              'show_indexes': True}),
                            # {'document_root': settings.MEDIA_URL +
                            # {'document_root' : os.path.dirname(__file__) +
                            # '/static'}),
    )
